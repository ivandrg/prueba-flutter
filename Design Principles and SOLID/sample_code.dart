// The following code violates the Dependency Inversion Principle.
// Refactor the code to conform to the DIP.

import 'dart:html';

class UserService {
  Database _database;

  UserService(this._database);

  void registerUser(String username, String password) {
    // Code to register a new user in the database
    _database.saveUser(username, password);
  }
}

abstract class Database {
  void saveUser(String username, String password) {}
}

class DatabaseImpl implements Database {
  @override
  void saveUser(String username, String password) {
    // TODO: implement saveUser
  }
}
