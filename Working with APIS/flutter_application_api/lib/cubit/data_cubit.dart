import 'package:bloc/bloc.dart';
import 'package:flutter_application_api/model/data_model.dart';
import 'package:meta/meta.dart';

import '../data/datasource.dart';

part 'data_state.dart';

class DataCubit extends Cubit<DataState> {
  DataCubit() : super(DataInitial());
  final datasource = RemoteDatasource();
  Future<void> postData() async {
    try {
      final response = await datasource.postData();
      emit(DataLoaded(response!));
    } on Faliure catch (e) {
      emit(DataError(e.message));
    }
  }
}
