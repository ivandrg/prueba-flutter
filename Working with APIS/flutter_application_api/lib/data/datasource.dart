import 'dart:convert';

import 'package:flutter_application_api/model/data_model.dart';
import 'package:http/http.dart' as http;

class RemoteDatasource {
  Future<DataModel?> postData() async {
    try {
      var headers = {
        'x-api-key': '4d718494-b7ff-4b42-9b69-c31f41f4b7ec',
      };
      var url =
          Uri.https('development.api.netforemost.com', 'api/auth/app-client');
      var response = await http.post(
        url,
        body: {
          "appName": "string",
          "clientName": "string",
          "description": "string"
        },
        headers: headers,
      );
      if (response.statusCode == 200) {
        DataModel data = DataModel.fromJson(jsonDecode(response.body));
        return data;
      } else if (response.statusCode == 401) {
        throw Faliure(response.body);
      } else if (response.statusCode == 500) {
        throw Faliure(response.body);
      }
    } on Faliure catch (e) {
      throw Faliure(e.message);
    }
    return null;
  }
}

class Faliure {
  final String message;

  Faliure(this.message);
}
