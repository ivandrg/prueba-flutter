import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_application_api/cubit/data_cubit.dart';
import 'package:flutter_application_api/data/datasource.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DataCubit(),
      child: MaterialApp(
        title: 'API Test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: APITestPage(),
      ),
    );
  }
}

class APITestPage extends StatefulWidget {
  @override
  _APITestPageState createState() => _APITestPageState();
}

class _APITestPageState extends State<APITestPage> {
  String _apiResponse = '';

  Future<void> fetchData() async {
    const url =
        'https://development.api.netforemost.com/api/auth/app-client'; // Replace with your API endpoint
    try {
      final response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        final data = json.decode(response.body);
        setState(() {
          _apiResponse = data.toString();
        });
      } else {
        setState(() {
          _apiResponse = 'Error: ${response.statusCode}';
        });
      }
    } catch (e) {
      setState(() {
        _apiResponse = 'Error: $e';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('API Test'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: fetchData,
              child: Text('Fetch Data'),
            ),
            SizedBox(height: 16),
            Text(
              _apiResponse,
              style: TextStyle(fontSize: 18),
            ),
            ElevatedButton(
              onPressed: () {
                context.read<DataCubit>().postData();
              },
              child: Text('Post Data'),
            ),
            BlocBuilder<DataCubit, DataState>(
              builder: (context, state) {
                if (state is DataLoaded) {
                  return Text(
                      '${state.data.appName} ${state.data.clientName} ${state.data.description}');
                }
                return Container();
              },
            )
          ],
        ),
      ),
    );
  }
}
