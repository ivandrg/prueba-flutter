// To parse this JSON data, do
//
//     final dataModel = dataModelFromJson(jsonString);

import 'dart:convert';

DataModel dataModelFromJson(String str) => DataModel.fromJson(json.decode(str));

String dataModelToJson(DataModel data) => json.encode(data.toJson());

class DataModel {
  String appName;
  String clientName;
  String description;

  DataModel({
    required this.appName,
    required this.clientName,
    required this.description,
  });

  factory DataModel.fromJson(Map<String, dynamic> json) => DataModel(
        appName: json["appName"],
        clientName: json["clientName"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "appName": appName,
        "clientName": clientName,
        "description": description,
      };
}
